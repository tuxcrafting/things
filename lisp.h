#pragma once

#include <stdlib.h>
#include <stdio.h>

// Applications
#define _(x, ...) x(__VA_ARGS__)
#define _1(op, x) (op x)
#define _I(op, x) (x op)
#define _2(op, x, y) (x op y)

// Literals
#define quote(...) (__VA_ARGS__)
#define block(...) { __VA_ARGS__; }

// Variables
#define scalar(type, name) type name
#define array(type, name, length) type name[length]
#define define(variable, value) variable = value

// Types
#define string char*
#define int8 signed char
#define uint8 unsigned char
#define int16 signed short
#define uint16 unsigned short
#define int32 signed long
#define uint32 unsigned long
#define int64 signed long long
#define uint64 unsigned long long
#define int signed size_t
#define uint unsigned size_t

// Control flow
#define iff(condition, yes, no) condition ? yes : no
#define cond(condition, body, other) if (condition) body else other
#define loop(initial, condition, post, body) for (initial; condition; post) body

// Structures
#define sub(type, name, args, body) type name args body
#define record(name, body) typedef struct { body; } name;