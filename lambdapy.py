# lambdapy
# abstract lambda calculus things in python

I = lambda x: x
K = lambda x: lambda y: x
S = lambda x: lambda y: lambda z: x(z)(y(z))
X = lambda x: x(S)(K)
C = lambda f: lambda g: lambda x: f(x)(g)
B = lambda f: lambda g: lambda x: f(g(x))
W = lambda x: lambda y: x(y)(y)
Y = lambda g: (lambda x: g(x(x)))((lambda x: g(x(x))))

def ntol(n):
    z = ZERO
    for _ in range(n):
        z = SUCC(z)
    return z
def lton(l):
    return l(lambda x: x + 1)(0)
def btol(b):
    return [FALSE, TRUE][b]
def ltob(l):
    return l(True)(False)

ZERO = lambda f: lambda x: x
SUCC = lambda n: lambda f: lambda x: f(n(f)(x))
PLUS = lambda m: lambda n: lambda f: lambda x: m(f)(n(f)(x))
MULT = lambda m: lambda n: lambda f: m(n(f))
POW = lambda b: lambda e: e(b)
PRED = lambda n: lambda f: lambda x: \
    n(lambda g: lambda h: h(g(f)))(lambda u: x)(lambda u: u)
SUB = lambda m: lambda n: n(PRED)(m)

PAIR = lambda x: lambda y: lambda f: f(x)(y)
FIRST = lambda p: p(TRUE)
SECOND = lambda p: p(FALSE)
NIL = lambda x: TRUE
NULL = lambda p: p(lambda x: lambda y: FALSE)

TRUE = lambda x: lambda y: x
FALSE = lambda x: lambda y: y
AND = lambda p: lambda q: p(q)(p)
OR = lambda p: lambda q: p(p)(q)
NOT = lambda p: p(FALSE)(TRUE)

IFTHENELSE = lambda p: lambda a: lambda b: p(a)(b)

ISZERO = lambda n: n(lambda x: FALSE)(TRUE)
LEQ = lambda m: lambda n: ISZERO(SUB(m)(n))
EQ = lambda m: lambda n: AND(LEQ(m)(n))(LEQ(n)(m))
