#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <term.h>
#include <unistd.h>

#define TERM_RESET				\
	printf("\033[0m");			\
	fprintf(stderr, "\033[0m")

#define ERRNO_DIE					\
	TERM_RESET;					\
	fprintf(stderr, "%s\n", strerror(errno));	\
	exit(EXIT_FAILURE)

FILE *instream;
int g_argc;
char **g_argv;

unsigned char ansi_color16[16][3] = {
	{ 0x00, 0x00, 0x00 },
	{ 0xCD, 0x00, 0x00 },
	{ 0x00, 0xCD, 0x00 },
	{ 0xCD, 0xCD, 0x00 },
	{ 0x00, 0x00, 0xEE },
	{ 0xCD, 0x00, 0xCD },
	{ 0x00, 0xCD, 0xCD },
	{ 0xE5, 0xE5, 0xE5 },
	{ 0x7F, 0x7F, 0x7F },
	{ 0xFF, 0x00, 0x00 },
	{ 0x00, 0xFF, 0x00 },
	{ 0xFF, 0xFF, 0x00 },
	{ 0x5C, 0x5C, 0xFF },
	{ 0xFF, 0x00, 0xFF },
	{ 0x00, 0xFF, 0xFF },
	{ 0xFF, 0xFF, 0xFF },
};

int get_next_stream() {
	if (optind >= g_argc) {
		return -1;
	}
	char *filename = g_argv[optind++];
	if (instream != stdin) {
		fclose(instream);
	}
	if (strcmp(filename, "-") == 0) {
		instream = stdin;
		return 0;
	}
	if ((instream = fopen(filename, "r")) == NULL) {
		ERRNO_DIE;
	}
	return 0;
}

int read_character() {
	int ch = fgetc(instream);
	if (ch == EOF) {
		if (ferror(instream)) {
			ERRNO_DIE;
		} else if (feof(instream)) {
			if (get_next_stream() == -1) {
				return -1;
			}
			return read_character();
		}
	}
	return ch;
}

struct color_index {
	int dist;
	int index;
};

char sbuf[128];

int color_cmp(const void *a, const void *b) {
	struct color_index *x = (struct color_index*)a;
	struct color_index *y = (struct color_index*)b;
	return x->dist - y->dist;
}

#define distance(x, y) ((x) - (y)) * ((x) - (y))
#define color_distance(r1, g1, b1, r2, g2, b2)	\
	distance(r1, r2) +			\
	distance(g1, g2) +			\
	distance(b1, b2)

char *get_ansi_code(int r, int g, int b, int mode) {
	if (mode <= 16) {
		struct color_index matches[16];
		for (int i = 0; i < mode; i++) {
			matches[i].dist = color_distance(
				r, g, b,
				ansi_color16[i][0],
				ansi_color16[i][1],
				ansi_color16[i][2]);
			matches[i].index = i;
		}
		qsort(matches, mode, sizeof(struct color_index), &color_cmp);
		int match_index = matches[0].index;
		if (match_index < 8) {
			sprintf(sbuf, "3%d", match_index);
		} else {
			sprintf(sbuf, "9%d", match_index - 8);
		}
	} else {
		int gray_possible = 1, gray;
		int color;
		double sep = 2.5;

		while (gray_possible) {
			if (r < sep || g < sep || b < sep) {
				gray = r < sep && g < sep && b < sep;
				gray_possible = 0;
			}
			sep += 42.5;
		}

		if (gray) {
			color = 232 + (r + g + b) / 33;
		} else {
#define to_cube(n) (int)round((n) / 255.0 * 5.0)
			color = 16 +
				to_cube(r) * 36 +
				to_cube(g) * 6 +
				to_cube(b);
		}

		sprintf(sbuf, "38;5;%d", color);
	}
	return sbuf;
}

void print_usage(FILE *stream) {
	fprintf(stream, "Usage: %s [option...] [file...]\n", g_argv[0]);
}

void print_help() {
	puts("Options:");
	puts(" -h          show this help message and exit");
	puts(" -p spread   rainbow spread");
	puts(" -F freq     rainbow frequency");
	puts(" -S seed     rainbow seed");
	puts(" -3          force 8 colors mode");
	puts(" -4          force 16 colors mode");
	puts(" -8          force 256 colors mode");
	puts(" -f          ignored");
	puts(" -c charset  ignored");
}

int get_mode() {
	char *term = getenv("TERM");
	if (term == NULL || strlen(term) == 0 || tgetent(NULL, term) != 1) {
		return 8;
	}

	int mode = tgetnum("colors");
	if (mode == -1) {
		mode = 8;
	}
	return mode;
}

int main(int argc, char *argv[]) {
	g_argc = argc;
	g_argv = argv;
	int opt;

	double spread = 3.0;
	double freq = 0.1;
	double seed = 0;

	int mode = get_mode();

	while ((opt = getopt(argc, argv, "hpFS348fc")) != -1) {
		switch (opt) {
		case 'h':
			print_usage(stdout);
			print_help();
			exit(EXIT_SUCCESS);
		case 'p':
			spread = atof(argv[optind++]);
			break;
		case 'F':
			freq = atof(argv[optind++]);
			break;
		case 'S':
			seed = atof(argv[optind++]);
			break;
		case '3':
			mode = 8;
			break;
		case '4':
			mode = 16;
			break;
		case '8':
			mode = 256;
			break;
		case 'f': // kept for compatibility with python lolcat
			break;
		case 'c': // idem
			optind++;
			break;
		default:
			print_usage(stderr);
			exit(EXIT_FAILURE);
		}
	}
	instream = stdin;
	if (optind < argc) {
		get_next_stream();
	}
	int ch;
	double i = seed;
	while ((ch = read_character()) != -1) {
#define int_sin(n) (int)(sin(n) * 127 + 128)
		int r = int_sin(freq * i);
		int g = int_sin(freq * i + 2 * M_PI / 3);
		int b = int_sin(freq * i + 4 * M_PI / 3);
		printf("\033[%sm", get_ansi_code(r, g, b, mode));
		fputc(ch, stdout);
		i += spread;
	}
	TERM_RESET;
	return EXIT_SUCCESS;
}
