# Telechat
# IRC but it's telnet and actually not IRC

from twisted.internet import protocol, reactor, endpoints

LEADER = ""
TRAILER = "\r\n"

class Telechat(protocol.Protocol):
    def __init__(self, factory):
        self.factory = factory
        self.name = None

    def connectionMade(self):
        self.factory.clients.add(self)
        self.transport.write(b"What is your name? ")

    def connectionLost(self, reason):
        self.factory.clients.remove(self)
        if self.name:
            self.factory.broadcast(self, "[%s left]" % self.name)

    def dataReceived(self, data):
        if data == b"\x04":
            self.transport.loseConnection()
            return
        try:
            if not self.name:
                self.name = data.decode("utf8").strip()
                self.transport.write(b"Hello!\r\n")
                self.factory.broadcast(self, "[%s joined]" % self.name)
            else:
                data = data.decode("utf8").rstrip()
                self.factory.broadcast(self, "<%s> %s" % (self.name, data))
        except:
            pass

class TelechatFactory(protocol.Factory):
    def __init__(self):
        self.clients = set()

    def broadcast(self, sender, msg):
        print(msg)
        msg = LEADER + msg + TRAILER
        for c in self.clients:
            c.transport.write(msg.encode("utf8"))

    def buildProtocol(self, addr):
        return Telechat(self)

print("Listening on *:9292")
endpoints.serverFromString(reactor, "tcp:9292").listen(TelechatFactory())
reactor.run()
