# DTMFstream
# Simple network stream of infinite random DTMF data
# Q: Why?
# A: Why not?

import math
import random
import struct
import time
from twisted.internet import protocol, reactor, endpoints, task

DTMF_H = [1209, 1336, 1477, 1633]
DTMF_L = [697, 770, 852, 941]

CH_TO_DTMF = {
    "0": (3, 1),
    "1": (0, 0),
    "2": (0, 1),
    "3": (0, 2),
    "4": (1, 0),
    "5": (1, 1),
    "6": (1, 2),
    "7": (2, 0),
    "8": (2, 1),
    "9": (2, 2),
}

class DtmfStream(protocol.Protocol):
    def __init__(self, factory):
        self.factory = factory
        self.name = None

    def connectionMade(self):
        self.factory.clients.add(self)

    def connectionLost(self, reason):
        self.factory.clients.remove(self)

DELAY = 0.05

class DtmfStreamFactory(protocol.Factory):
    def __init__(self):
        self.clients = set()
        self.lc = task.LoopingCall(self.genSample)
        self.lc.start(0.25)

    def genSample(self):
        i = time.time()
        l = []
        for _ in range(2000):
            m = i // DELAY % 2
            if m:
                random.seed(i // DELAY)
                n = random.randint(0, 9)
                h_i, l_i = CH_TO_DTMF[str(n)]
                s_h = math.sin(i * 2 * math.pi * DTMF_H[h_i])
                s_l = math.sin(i * 2 * math.pi * DTMF_L[l_i])
                l.append(int((s_h / 2 + s_l / 2) * 32767))
            else:
                l.append(0)
            i += 1 / 8000
        b = struct.pack("2000h", *l)
        if not self.clients:
            return
        for client in self.clients:
            client.transport.write(b)

    def buildProtocol(self, addr):
        return DtmfStream(self)

print("Listening on *:2992")
endpoints.serverFromString(reactor, "tcp:2992").listen(DtmfStreamFactory())
reactor.run()
