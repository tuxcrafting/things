#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lexer.h"

void cc_error(char *message);

FILE *lex_stream;

int ch = ' ';

int _is_digit(char c) {
    return c >= '0' && c <= '9';
}
int _is_digit_8(char c) {
    return c >= '0' && c <= '7';
}
int _is_alpha(char c) {
    return (c >= 'a' && c <= 'z')
        || (c >= 'A' && c <= 'Z')
        || c == '_';
}
int _is_whitespace(char c) {
    return c > 0 && c <= ' ';
}

int _next() {
    char c;
    if (fread(&c, 1, 1, lex_stream) < 1) {
        if (ferror(lex_stream)) {
            cc_error("error while reading stream\n");
        }
        return -1;
    }
    return c;
}

char _eval_escape() {
    ch = _next();
    if (_is_digit_8(ch)) {
        int num = 0;
        while (_is_digit_8(ch)) {
            num = (num * 8) + (ch - '0');
            ch = _next();
        }
    }
    switch (ch) {
    case 'n':
        return '\n';
    case 't':
        return '\t';
    case 'v':
        return '\v';
    case 'f':
        return '\f';
    case 'r':
        return '\r';
    case 'a':
        return '\a';
    case '"':
    case '\'':
    case '\\':
        return ch;
    default:
        cc_error("unknown escape\n");
    }
}

char *keywords[] = {
    // types
    "void",
    "int",
    "long",
    "short",
    "unsigned",
    "char",

    // control flow
    "if",
    "else",
    "switch",
    "case",
    "do",
    "while",
    "for",
    "return",

    // data structure
    "struct",
    "enum",
    "union",

    // else
    "typedef",
    "sizeof",
    "extern",
};

char single_char_tokens[] = "~?:;.,(){}[]";

int lex_token_next(void *value) {
    while (_is_whitespace(ch)) {
        ch = _next();
    }
    if (ch == -1) {
        return TOK_EOF;
    }
    if (_is_digit(ch)) {
        int num = 0;
        while (_is_digit(ch)) {
            num = (num * 10) + (ch - '0');
            ch = _next();
        }
        *(int*)value = num;
        return TOK_INTEGER;
    }
    if (_is_alpha(ch)) {
        int buffer_size = 16, buffer_index = 0;
        char *buffer = malloc(buffer_size);
        while (_is_alpha(ch) || _is_digit(ch)) {
            buffer[buffer_index++] = ch;
            if (buffer_index + 1 == buffer_size) {
                buffer_size *= 2;
                buffer = realloc(buffer, buffer_size);
            }
            ch = _next();
        }
        buffer[buffer_index] = 0;
        *(char**)value = buffer;
        for (int i = 0; i < sizeof(keywords) / sizeof(char*); i++) {
            if (strcmp(buffer, keywords[i]) == 0) {
                return TOK_GENERIC;
            }
        }
        return TOK_IDENTIFIER;
    }
    if (ch == '\'') {
        ch = _next();
        if (ch == -1) {
            cc_error("unexpected EOF\n");
        }
        if (ch == '\\') {
            *(char*)value = _eval_escape();
        } else {
            *(char*)value = ch;
        }
        if (_next() != '\'') {
            cc_error("invalid token\n");
        }
        ch = _next();
        return TOK_CHAR;
    }
    if (ch == '"') {
        ch = _next();
        int buffer_size = 16, buffer_index = 0;
        char *buffer = malloc(buffer_size);
        while (ch != '"') {
            if (ch == -1) {
                cc_error("unexpected EOF\n");
            }
            if (ch == '\\') {
                buffer[buffer_index++] = _eval_escape();
            } else {
                buffer[buffer_index++] = ch;
            }
            if (buffer_index + 1 == buffer_size) {
                buffer_size *= 2;
                buffer = realloc(buffer, buffer_size);
            }
            ch = _next();
        }
        buffer[buffer_index] = 0;
        *(char**)value = buffer;
        ch = _next();
        return TOK_STRING;
    }
#define TWO_CHAR_TOKEN(a, b) \
    if (a) { \
        int tmp = ch; \
        ch = _next(); \
        if (b) { \
            char *buffer = malloc(3); \
            buffer[0] = tmp; \
            buffer[1] = ch; \
            buffer[2] = 0; \
            *(char**)value = buffer; \
            ch = _next(); \
            return TOK_GENERIC; \
        } else { \
            char *buffer = malloc(2); \
            buffer[0] = tmp; \
            buffer[1] = 0; \
            *(char**)value = buffer; \
            return TOK_GENERIC; \
        } \
    }
    if (ch == '>' || ch == '<') {
        int tmp = ch;
        ch = _next();
        if (ch == tmp) {
            ch = _next();
            if (ch == '=') {
                char *buffer = malloc(4);
                buffer[0] = tmp;
                buffer[1] = tmp;
                buffer[2] = '=';
                buffer[3] = 0;
                *(char**)value = buffer;
                ch = _next();
                return TOK_GENERIC;
            } else {
                char *buffer = malloc(3);
                buffer[0] = tmp;
                buffer[1] = tmp;
                buffer[2] = 0;
                *(char**)value = buffer;
                return TOK_GENERIC;
            }
        } else {
            char *buffer = malloc(2);
            buffer[0] = tmp;
            buffer[1] = 0;
            *(char**)value = buffer;
            return TOK_GENERIC;
        }
    }
    TWO_CHAR_TOKEN(ch == '-', ch == '-' || ch == '>' || ch == '=');
    TWO_CHAR_TOKEN(ch == '+', ch == '+' || ch == '=');
    TWO_CHAR_TOKEN(ch == '*'
                || ch == '/'
                || ch == '%'
                || ch == '!'
                || ch == '='
                || ch == '^', ch == '=');
    TWO_CHAR_TOKEN(ch == '&', ch == '&' || ch == '=');
    TWO_CHAR_TOKEN(ch == '|', ch == '|' || ch == '=');
    for (int i = 0; i < sizeof(single_char_tokens); i++) {
        if (ch == single_char_tokens[i]) {
            char *buffer = malloc(2);
            buffer[0] = ch;
            buffer[1] = 0;
            *(char**)value = buffer;
            ch = _next();
            return TOK_GENERIC;
        }
    }
    if (!_is_whitespace(ch)) {
        cc_error("invalid token\n");
    }
}
