#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "parser.h"

void cc_error(char *message) {
    fprintf(stderr, message);
    exit(1);
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s infile\n", argv[0]);
        exit(1);
    }
    lex_stream = fopen(argv[1], "r");
    if (lex_stream == NULL) {
        cc_error("couldn't open file\n");
    }
    parser_token_next();
    parser_pprint(parser_expr());
    return 0;
}
