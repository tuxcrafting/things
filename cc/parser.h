#pragma once

#include "lexer.h"

#define AST_INTEGER 0
#define AST_IDENTIFIER 1
#define AST_STRING 2
#define AST_UNOP 3
#define AST_BINOP 4
#define AST_TERNARY 5
#define AST_ASSIGN 6
#define AST_INDEX 7
#define AST_CALL 8
#define AST_MEMBER 9
#define AST_SIZEOF 10
#define AST_TYPE 11
#define AST_TYPEDEF 12

typedef union {
    int vInt;
    char *vStr;
    char vChar;
} token_value_t;

typedef union {
    struct ast_node_t *vNode;
    int vInt;
    char vChar;
    char *vStr;
} ast_value_t;

typedef struct ast_node_t {
    int type;
    ast_value_t *children;
} ast_node_t;

extern token_value_t token_value;
extern int token_type;

#define parser_expr parser_expr13

void parser_pprint(ast_node_t*);
void parser_token_next();
ast_node_t *parser_expr13();
ast_node_t *parser_expr12();
ast_node_t *parser_expr11();
ast_node_t *parser_expr10();
ast_node_t *parser_expr9();
ast_node_t *parser_expr8();
ast_node_t *parser_expr7();
ast_node_t *parser_expr6();
ast_node_t *parser_expr5();
ast_node_t *parser_expr4();
ast_node_t *parser_expr3();
ast_node_t *parser_expr2();
ast_node_t *parser_expr1();
ast_node_t *parser_expr0();
ast_node_t *parser_lvalue1();
ast_node_t *parser_lvalue0();
ast_node_t *parser_type();
ast_node_t *parser_ident();
