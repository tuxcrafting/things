#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "parser.h"

void cc_error(char *message);

token_value_t token_value;
int token_type;

void parser_pprint(ast_node_t *node) {
    switch (node->type) {
    case AST_INTEGER:
        printf("AST_INTEGER(%d)", node->children[0].vInt);
        break;
    case AST_IDENTIFIER:
        printf("AST_IDENTIFIER(%s)", node->children[0].vStr);
        break;
    case AST_STRING:
        printf("AST_STRING(%s)", node->children[0].vStr);
        break;
    case AST_UNOP:
        printf("AST_UNOP[%c](", node->children[0].vChar);
        parser_pprint(node->children[1].vNode);
        printf(")");
        break;
    case AST_BINOP:
        printf("AST_BINOP[%c](", node->children[0].vChar);
        parser_pprint(node->children[1].vNode);
        printf(", ");
        parser_pprint(node->children[2].vNode);
        printf(")");
        break;
    case AST_TERNARY:
        printf("AST_TERNARY(");
        parser_pprint(node->children[0].vNode);
        printf(", ");
        parser_pprint(node->children[1].vNode);
        printf(", ");
        parser_pprint(node->children[2].vNode);
        printf(")");
        break;
    case AST_ASSIGN:
        printf("AST_ASSIGN[%c](", node->children[0].vChar);
        parser_pprint(node->children[1].vNode);
        printf(", ");
        parser_pprint(node->children[2].vNode);
        printf(")");
        break;
    case AST_INDEX:
        printf("AST_INDEX(");
        parser_pprint(node->children[0].vNode);
        printf(", ");
        parser_pprint(node->children[1].vNode);
        printf(")");
        break;
    case AST_CALL:
        printf("AST_CALL[");
        parser_pprint(node->children[0].vNode);
        printf("](");
        for (int i = 0; i < node->children[1].vInt; i++) {
            parser_pprint(node->children[2 + i].vNode);
            if (i != node->children[1].vInt - 1) {
                printf(", ");
            }
        }
        printf(")");
        break;
    case AST_MEMBER:
        printf("AST_MEMBER[%d](", node->children[0].vInt);
        parser_pprint(node->children[1].vNode);
        printf(", ");
        parser_pprint(node->children[2].vNode);
        printf(")");
        break;
    case AST_TYPE:
        printf("AST_TYPE[%c](", node->children[0].vChar);
        parser_pprint(node->children[1].vNode);
        printf(")");
        break;
    }
}

#define MAKE_NODE(t, a_t, a) \
    ast_node_t *_node = malloc(sizeof(ast_node_t)); \
    _node->type = t; \
    _node->children = malloc(sizeof(ast_value_t)); \
    _node->children[0].v##a_t = a
#define MAKE_NODE2(t, a_t, a, b_t, b) \
    ast_node_t *_node = malloc(sizeof(ast_node_t)); \
    _node->type = t; \
    _node->children = malloc(2 * sizeof(ast_value_t)); \
    _node->children[0].v##a_t = a; \
    _node->children[1].v##b_t = b
#define MAKE_NODE3(t, a_t, a, b_t, b, c_t, c) \
    ast_node_t *_node = malloc(sizeof(ast_node_t)); \
    _node->type = t; \
    _node->children = malloc(3 * sizeof(ast_value_t)); \
    _node->children[0].v##a_t = a; \
    _node->children[1].v##b_t = b; \
    _node->children[2].v##c_t = c
#define MAKE_NODE4(t, a_t, a, b_t, b, c_t, c, d_t, d) \
    ast_node_t *_node = malloc(sizeof(ast_node_t)); \
    _node->type = t; \
    _node->children = malloc(3 * sizeof(ast_value_t)); \
    _node->children[0].v##a_t = a; \
    _node->children[1].v##b_t = b; \
    _node->children[2].v##c_t = c; \
    _node->children[3].v##d_t = d

void parser_token_next() {
    token_type = lex_token_next(&token_value);
}

int parser_is_lvalue(ast_node_t *node) {
    if (node->type == AST_IDENTIFIER
     || node->type == AST_INDEX
     || node->type == AST_MEMBER) {
        return 1;
    }
    if (node->type == AST_UNOP) {
        return node->children[0].vChar == '*';
    }
    return 0;
}

void parser_enforce_lvalue(ast_node_t *node) {
    if (parser_is_lvalue(node) == 0) {
        cc_error("parse error\n");
    }
}

#define UNARY_OP(op_s, op_c, rule) \
    if (strcmp(token_value.vStr, op_s) == 0) { \
        parser_token_next(); \
        MAKE_NODE2(AST_UNOP, Char, op_c, Node, parser_##rule()); \
        return _node; \
    }

#define UNARY_OP_L(op_s, op_c, rule) \
    if (strcmp(token_value.vStr, op_s) == 0) { \
        parser_token_next(); \
        ast_node_t *node = parser_##rule(); \
        parser_enforce_lvalue(node); \
        MAKE_NODE2(AST_UNOP, Char, op_c, Node, node); \
        return _node; \
    }

#define BINARY_OP(op_s, op_c, rule) \
    if (strcmp(token_value.vStr, op_s) == 0) { \
        parser_token_next(); \
        MAKE_NODE3(AST_BINOP, Char, op_c, Node, node, Node, parser_##rule()); \
        node = _node; \
    }

#define ASSIGN(op_s, op_c) \
    if (strcmp(token_value.vStr, op_s) == 0) { \
        parser_enforce_lvalue(node); \
        parser_token_next(); \
        MAKE_NODE3(AST_ASSIGN, Char, op_c, \
                Node, node, Node, parser_expr()); \
        return _node; \
    }

#define ONE_OP_EXPR(op_s, op_c, a, b) \
    ast_node_t *parser_expr##a() { \
        ast_node_t *node = parser_expr##b(); \
        while (token_type == TOK_GENERIC) { \
            BINARY_OP(op_s, op_c, expr##b) \
            else { \
                break; \
            } \
        } \
        return node; \
    }

ast_node_t *parser_expr13() {
    ast_node_t *node = parser_expr12();
    if (token_type == TOK_GENERIC) {
        ASSIGN("=", '=')
        else ASSIGN("+=", '+')
        else ASSIGN("-=", '-')
        else ASSIGN("*=", '*')
        else ASSIGN("%=", '%')
        else ASSIGN("/=", '/')
        else ASSIGN("<<=", ')')
        else ASSIGN(">>=", '(')
        else ASSIGN("&=", '&')
        else ASSIGN("|=", '|')
        else ASSIGN("^=", '^');
    }
    return node;
}

ast_node_t *parser_expr12() {
    ast_node_t *node = parser_expr11();
    if (token_type == TOK_GENERIC) {
        if (strcmp(token_value.vStr, "?") == 0) {
            parser_token_next();
            ast_node_t *node2 = parser_expr();
            if (token_type != TOK_GENERIC
             || strcmp(token_value.vStr, ":") != 0) {
                cc_error("parse error\n");
            }
            parser_token_next();
            MAKE_NODE3(AST_TERNARY,
                    Node, node, Node, node2, Node, parser_expr12());
            return _node;
        }
    }
    return node;
}

ONE_OP_EXPR("||", 'O', 11, 10)
ONE_OP_EXPR("&&", 'A', 10, 9)
ONE_OP_EXPR("|", '|', 9, 8)
ONE_OP_EXPR("^", '^', 8, 7)
ONE_OP_EXPR("&", '&', 7, 6)

ast_node_t *parser_expr6() {
    ast_node_t *node = parser_expr5();
    while (token_type == TOK_GENERIC) {
        BINARY_OP("==", '=', expr5)
        else BINARY_OP("!=", '!', expr5)
        else {
            break;
        }
    }
    return node;
}

ast_node_t *parser_expr5() {
    ast_node_t *node = parser_expr4();
    while (token_type == TOK_GENERIC) {
        BINARY_OP("<", '<', expr4)
        else BINARY_OP(">", '>', expr4)
        else BINARY_OP("<=", '(', expr4)
        else BINARY_OP(">=", ')', expr4)
        else {
            break;
        }
    }
    return node;
}

ast_node_t *parser_expr4() {
    ast_node_t *node = parser_expr3();
    while (token_type == TOK_GENERIC) {
        BINARY_OP("<<", 'l', expr3)
        else BINARY_OP(">>", 'r', expr3)
        else {
            break;
        }
    }
    return node;
}

ast_node_t *parser_expr3() {
    ast_node_t *node = parser_expr2();
    while (token_type == TOK_GENERIC) {
        BINARY_OP("+", '+', expr2)
        else BINARY_OP("-", '-', expr2)
        else {
            break;
        }
    }
    return node;
}

ast_node_t *parser_expr2() {
    ast_node_t *node = parser_expr1();
    while (token_type == TOK_GENERIC) {
        BINARY_OP("*", '*', expr1)
        else BINARY_OP("/", '/', expr1)
        else BINARY_OP("%", '%', expr1)
        else {
            break;
        }
    }
    return node;
}

ast_node_t *parser_expr1() {
    if (token_type == TOK_GENERIC) {
        UNARY_OP("+", '+', expr1)
        else UNARY_OP("-", '-', expr1)
        else UNARY_OP("!", '!', expr1)
        else UNARY_OP("~", '~', expr1)
        else UNARY_OP_L("++", '>', expr1)
        else UNARY_OP_L("--", '<', expr1)
        else UNARY_OP_L("&", '&', expr1)
        else UNARY_OP("*", '*', expr1)
        else UNARY_OP("sizeof", 's', type)
    }
    return parser_expr0();
}

ast_node_t *parser_expr0() {
    ast_node_t *node;
    if (token_type == TOK_INTEGER) {
        MAKE_NODE(AST_INTEGER, Int, token_value.vInt);
        parser_token_next();
        node = _node;
    } else if (token_type == TOK_GENERIC) {
        if (strcmp(token_value.vStr, "(") == 0) {
            parser_token_next();
            node = parser_expr();
            if (token_type != TOK_GENERIC
             || strcmp(token_value.vStr, ")") != 0) {
                cc_error("parse error\n");
            }
            parser_token_next();
        }
    } else if (token_type == TOK_IDENTIFIER) {
        MAKE_NODE(AST_IDENTIFIER, Str, token_value.vStr);
        parser_token_next();
        node = _node;
    } else if (token_type == TOK_STRING) {
        MAKE_NODE(AST_STRING, Str, token_value.vStr);
        parser_token_next();
        node = _node;
    } else if (token_type == TOK_CHAR) {
        MAKE_NODE(AST_INTEGER, Int, token_value.vChar);
        parser_token_next();
        node = _node;
    }
    while (token_type == TOK_GENERIC) {
        if (strcmp(token_value.vStr, "++") == 0) {
            parser_enforce_lvalue(node);
            MAKE_NODE2(AST_UNOP, Char, ')', Node, node);
            parser_token_next();
            node = _node;
        } else if (strcmp(token_value.vStr, "--") == 0) {
            parser_enforce_lvalue(node);
            MAKE_NODE2(AST_UNOP, Char, '(', Node, node);
            parser_token_next();
            node = _node;
        } else if (strcmp(token_value.vStr, "[") == 0) {
            parser_token_next();
            MAKE_NODE2(AST_INDEX, Node, node, Node, parser_expr());
            if (token_type != TOK_GENERIC
             || strcmp(token_value.vStr, "]") != 0) {
                cc_error("parse error\n");
            }
            parser_token_next();
            node = _node;
        } else if (strcmp(token_value.vStr, "(") == 0) {
            parser_token_next();
            ast_node_t *_node = malloc(sizeof(ast_node_t*));
            _node->type = AST_CALL;
            _node->children = malloc(16 * sizeof(ast_value_t));
            _node->children[0].vNode = node;
            int ch_i = 2, ch_len = 16;
            for (;;) {
                node = parser_expr();
                if (token_type == TOK_GENERIC) {
                    if (strcmp(token_value.vStr, ",") == 0
                     || strcmp(token_value.vStr, ")") == 0) {
                        _node->children[ch_i++].vNode = node;
                        if (ch_i == ch_len) {
                            ch_len *= 2;
                            _node->children = realloc(_node->children,
                                    ch_len * sizeof(ast_value_t));
                        }
                    } else {
                        cc_error("parse error\n");
                    }
                    if (strcmp(token_value.vStr, ")") == 0) {
                        break;
                    }
                } else {
                    cc_error("parse error\n");
                }
                parser_token_next();
            }
            parser_token_next();
            _node->children[1].vInt = ch_i - 2;
            node = _node;
        } else if (strcmp(token_value.vStr, ".") == 0) {
            parser_token_next();
            MAKE_NODE3(AST_MEMBER, Int, 0, Node, node, Node, parser_ident());
            node = _node;
        } else if (strcmp(token_value.vStr, "->") == 0) {
            parser_token_next();
            MAKE_NODE3(AST_MEMBER, Int, 1, Node, node, Node, parser_ident());
            node = _node;
        } else {
            break;
        }
    }
    return node;
}

ast_node_t *parser_type() {
    ast_node_t *node;
    if (token_type == TOK_IDENTIFIER) {
        MAKE_NODE(AST_IDENTIFIER, Str, token_value.vStr);
        parser_token_next();
        node = _node;
    } else if (token_type == TOK_GENERIC) {
        char t = ' ';
        if (strcmp(token_value.vStr, "struct") == 0) {
            t = 's';
        } else if (strcmp(token_value.vStr, "enum") == 0) {
            t = 'e';
        } else if (strcmp(token_value.vStr, "struct") == 0) {
            t = 's';
        } else if (strcmp(token_value.vStr, "(") == 0) {
            parser_token_next();
            node = parser_type();
            if (token_type != TOK_GENERIC
             || strcmp(token_value.vStr, ")") != 0) {
                cc_error("parse error\n");
            }
            parser_token_next();
        } else {
            cc_error("parse error\n");
        }
        if (t != ' ') {
            parser_token_next();
            MAKE_NODE2(AST_TYPE, Char, t, Node, parser_ident());
            node = _node;
        }
    } else {
        cc_error("parse error\n");
    }
    while (token_type == TOK_GENERIC) {
        if (strcmp(token_value.vStr, "*") == 0) {
            parser_token_next();
            MAKE_NODE2(AST_TYPE, Char, '*', Node, node);
            node = _node;
        } else {
            break;
        }
    }
    return node;
}

ast_node_t *parser_ident() {
    if (token_type == TOK_IDENTIFIER) {
        MAKE_NODE(AST_IDENTIFIER, Str, token_value.vStr);
        parser_token_next();
        return _node;
    }
    cc_error("parse error\n");
}
