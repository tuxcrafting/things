#pragma once

#define TOK_EOF 0
#define TOK_GENERIC 1
#define TOK_INTEGER 2
#define TOK_IDENTIFIER 3
#define TOK_CHAR 4
#define TOK_STRING 5

extern FILE *lex_stream;
int lex_token_next(void*);
