import json
from mastodon import Mastodon
import os
from PIL import Image
import random

cfg = json.load(open('config.json', 'r'))

client = Mastodon(
  client_id=cfg['client']['id'],
  client_secret=cfg['client']['secret'], 
  access_token=cfg['secret'], 
  api_base_url=cfg['site'])

def check_uniform(f):
    img = Image.open(f)
    last = img.getpixel((0, 0))

    for x in range(img.width):
        for y in range(img.height):
            if not (x == 0 and y == 0):
                if img.getpixel((x, y)) != last:
                    return False

    return True

def try_gen():
    seed = random.randint(0, 2**64-1)
    regnum = random.randint(6, 12)
    fname = "%d-%d-512x512-4x4-0x0.png" % (regnum, seed)
    cmdline = "seed=%d regnum=%d crange=4x4" % (seed, regnum)

    print("Attempting to generate %s (%s)" % (fname, cmdline))

    os.system("python3.6 genart.py %s show=n res=512x512" % cmdline)

    return fname, cmdline

while 1:
    f, c = try_gen()
    if check_uniform(f):
        os.unlink(f)
    else:
        break

client.status_post(c, media_ids=[client.media_post(f)])
os.unlink(f)
