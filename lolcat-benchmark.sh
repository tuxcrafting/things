#!/bin/sh

gcc lolcat.c -O3 -lm -lcurses

echo 'Generating test data'
dd if=/dev/urandom bs=1K count=2K | base64 > /tmp/test.txt

echo 'Running lolcat.py'
time lolcat /tmp/test.txt > /dev/null

echo 'Running lolcat.c'
time ./a.out /tmp/test.txt > /dev/null

echo 'Cleaning up'
rm a.out /tmp/test.txt
