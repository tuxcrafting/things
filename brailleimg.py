from PIL import Image
import sys

args = {
    "width": 124,
    "height": 124,
    "invert": 0,
}

for x in sys.argv[2:]:
    a = x.split(":")
    args[a[0]] = eval(a[1])

w = args["width"]
h = args["height"]

if w == 0 or h == 0:
    img = Image.open(sys.argv[1]).convert("1")
else:
    img = Image.open(sys.argv[1]).resize((w, h)).convert("1")

for y in range(img.height // 4):
    for x in range(img.width // 2):
        p00 = (img.getpixel((x * 2 + 0, y * 4 + 0)) != 0) ^ args["invert"]
        p01 = (img.getpixel((x * 2 + 0, y * 4 + 1)) != 0) ^ args["invert"]
        p02 = (img.getpixel((x * 2 + 0, y * 4 + 2)) != 0) ^ args["invert"]
        p03 = (img.getpixel((x * 2 + 0, y * 4 + 3)) != 0) ^ args["invert"]
        p10 = (img.getpixel((x * 2 + 1, y * 4 + 0)) != 0) ^ args["invert"]
        p11 = (img.getpixel((x * 2 + 1, y * 4 + 1)) != 0) ^ args["invert"]
        p12 = (img.getpixel((x * 2 + 1, y * 4 + 2)) != 0) ^ args["invert"]
        p13 = (img.getpixel((x * 2 + 1, y * 4 + 3)) != 0) ^ args["invert"]
        print(end=chr(0x2800 + \
            (p00 << 0) + \
            (p01 << 1) + \
            (p02 << 2) + \
            (p10 << 3) + \
            (p11 << 4) + \
            (p12 << 5) + \
            (p03 << 6) + \
            (p13 << 7)))
    print()
