# Script to check how many people you follow are blocked by dzuk's blockchain

# Usage:
# $ python blocked.py < path/to/follows.csv


blockchain = [
    ("Security / privacy risks", [
        "pleroma.rareome.ga",
    ]),
    ("Harassment / direct abuse to other people", [
        "2hu.club",
        "ap.uwu.st",
        "noagendasocial.com",
        "freespeech.firedragonstudios.com",
        "homura.space",
        "kowai.youkai.town",
        "loli.estate",
        "pleroma.yorha.club",
        "social.super-niche.club",
        "thechad.zone",
        "vampire.estate",
        "youkai.town",
    ]),
    ("Spam", [
        "2.distsn.org",
    ]),
    ("Violent speech and zero-moderation spaces", [
        "comm.network",
        "community.highlandarrow.com",
        "explosion.party",
        "freezepeach.xyz",
        "gs.mon5t3r.info",
        "jabb.in",
        "karolat.press",
        "pl.smuglo.li",
        "pleroma.cucked.me",
        "pleroma.rareome.ga",
        "pleroma.soykaf.com",
        "pridelands.io",
        "qoto.org",
        "rainbowdash.net",
        "sealion.club",
        "shitasstits.life",
        "shitposter.club",
        "social.au2pb.net",
        "social.i2p.rocks",
        "social.guizzyordi.info",
        "social.hidamari.blue",
        "social.lucci.xyz",
        "thechad.zone",
        "unsafe.space",
        "voluntaryism.club",
        "waifu.social",
        "weeaboo.space",
        "wrongthink.net",
        "woofer.alfter.us",
        "zerohack.xyz",
    ]),
    ("Dangerous/violent conspiracy theories", [
        "jabb.in",
        "not.phrack.fyi",
        "social.lucci.xyz",
        "explosion.party",
        "voluntaryism.club",
    ]),
    ("Unwilling to moderate hate speech and violent speech", [
        "cofe.moe",
        "freehold.earth",
        "ika.moe",
        "manx.social",
        "niu.moe",
        "social.targaryen.house",
        "social.wiuwiu.de",
        "toot.love",
    ]),
    ("Other hostile admin conduct", [
        "pridelands.io",
        "counter.social",
    ]),
    ("Illustrated sexualised depictions of people who appear to be minors", [
        "2hu.club",
        "baraag.net",
        "loli.estate",
        "pawoo.net"
        "shitasstits.life",
        "social.homunyan.com"
        "social.super-niche.club",
        "vampire.estate",
        "weeaboo.space",
        "wxw.moe",
        "youkai.town",
        "kowai.youkai.town",
    ]),
    ("Advertising", [
        "social.raptorengineering.io",
    ]),
]

def is_blocked(l):
    l = l.split("@")[-1]
    for x, y in blockchain:
        if l in y:
            return x

total = 0
blocked = 0
instances = {}
reasons = {}
for x in filter(bool, open(0).read().split("\n")):
    total += 1
    y = is_blocked(x)
    if y:
        i = x.split("@")[1]
        if i not in instances:
            instances[i] = 1
        else:
            instances[i] += 1
        if y not in reasons:
            reasons[y] = 1
        else:
            reasons[y] += 1
        blocked += 1
        print("%s: %s" % (x, y))

import operator
print()
print("Blocked: %d/%d (%f%%)" % (blocked, total, blocked / total * 100))
print("Top blocked instances:")
for x, y in reversed(sorted(instances.items(), key=operator.itemgetter(1))):
    print("- %s: %s" % (x, y))
print("Top reasons:")
for x, y in reversed(sorted(reasons.items(), key=operator.itemgetter(1))):
    print("- %s: %s" % (x, y))
