import cmath
from PIL import Image
import random
import struct
import sys

OPTIONS = {
    "regnum": "12",
    "seed": str(random.randint(0, 2**64-1)),
    "res": "192x192",
    "autoscale": "y",
    "crange": "1x1",
    "ccenter": "0x0",
    "show": "y",
}

if len(sys.argv) == 2 and sys.argv[1] == "-h":
    print("usage: %s [name=value]..." % sys.argv[1])
    print("options:")
    print("\tregnum = number of registers (default 12)")
    print("\tseed = rng seed (default random)")
    print("\tres = resolution (default 192x192)")
    print("\tautoscale = automatically scale complex plane (default y)")
    print("\tcrange = complex plane range (default 1x1)")
    print("\tccenter = complex plane center (default 0x0)")
    print("\tshow = show the generated image (default y)")
    sys.exit(0)

for x in sys.argv[1:]:
    s = x.split("=")
    a = s[0]
    b = "=".join(s[1:])
    OPTIONS[a] = b

REGNUM = int(OPTIONS["regnum"])
INSTRUCTIONS = {
    "add": lambda a, b: "%s + %s" % (a, b),
    "sub": lambda a, b: "%s - %s" % (a, b),
    "mul": lambda a, b: "%s * %s" % (a, b),
    "div": lambda a, b: "%s / %s" % (a, b),

    "neg": lambda a: "-%s" % a,
    "comp": lambda a: "(1+0j) / %s" % a,
    "conj": lambda a: "%s.conjugate()" % a,
    "abs": lambda a: "abs(%s) + 0j" % a,

    "sin": lambda a: "cmath.sin(%s)" % a,
    "cos": lambda a: "cmath.cos(%s)" % a,
    "tan": lambda a: "cmath.tan(%s)" % a,

    "asin": lambda a: "cmath.asin(%s)" % a,
    "acos": lambda a: "cmath.acos(%s)" % a,
    "atan": lambda a: "cmath.atan(%s)" % a,

    "logn": lambda a: "cmath.log(%s, cmath.e) + 0j" % a,
    "log2": lambda a: "cmath.log(%s, 2) + 0j" % a,
    "log10": lambda a: "cmath.log(%s, 10) + 0j" % a,
    "logx": lambda a, b: "cmath.log(%s, %s) + 0j" % (a, b),

    "expn": lambda a: "cmath.e ** %s + 0j" % a,
    "exp2": lambda a: "2 ** %s + 0j" % a,
    "exp10": lambda a: "10 ** %s + 0j" % a,
    "expx": lambda a, b: "%s ** %s" % (a, b),
}
INSLIST = list(INSTRUCTIONS.keys()) + ["li"]

class Random:
    def seed(self, seed):
        self.seed = seed
    def rand(self):
        x = self.seed
        x ^= x >> 12
        x ^= x << 25
        x ^= x >> 27
        self.seed = x
        return x * 0x2545F4914F6CDD1D

def generate(rng):
    i = 0
    while 1:
        if rng.rand() % 2**32 / 2**32 < i / 2**12:
            break
        opc = INSLIST[rng.rand() % len(INSLIST)]
        if opc == "li":
            a = struct.unpack("<d", struct.pack("<Q", rng.rand() % 2**64))[0]
            b = struct.unpack("<d", struct.pack("<Q", rng.rand() % 2**64))[0]
            yield ("li", rng.rand() % (REGNUM - 1) + 1, a+b*1j)
        else:
            a = map(lambda _: rng.rand() % REGNUM,
                    range(INSTRUCTIONS[opc].__code__.co_argcount))
            yield (opc, rng.rand() % (REGNUM - 1) + 1, *a)
        i += 1

def optimize_recurse(code, reg):
    if not code:
        return []
    i = len(code) - 1
    while i >= 0:
        x = code[i]
        if x[1] == reg:
            l = []
            for a in x[2:]:
                l += optimize_recurse(code[:i], a)
            return l + [i]
        i -= 1
    return []

def optimize(c):
    c = list(c)
    f = False
    i = len(c) - 1
    while i >= 0:
        x = c[i]
        if x[1] == 1:
            f = True
            c = c[:i + 1]
            break
        i -= 1
    if not f:
        return []
    l = []
    nc = []
    for x in optimize_recurse(c, 1):
        if x not in l:
            l.append(x)
            nc.append(c[x])
    return nc

def compile_code(code):
    s = "def compiled_asm(r0):\n"
    for i in range(1, REGNUM):
        s += "\tr%d = 0j\n" % i
    s += "\ttry:\n"
    for x in code:
        if x[0] == "li":
            s += "\t\tr%d = %s\n" % (x[1], repr(x[2]))
        else:
            s += "\t\tr%d = %s\n" % (x[1],
                    INSTRUCTIONS[x[0]](*map(lambda a: "r%d" % a, x[2:])))
    s += "\t\tr1 = abs(r1)\n"
    s += "\t\tif r1 > 1:\n"
    s += "\t\t\tr1 = 0\n"
    s += "\t\treturn int(r1 * 255)\n"
    s += "\texcept:\n"
    s += "\t\treturn 0\n"
    g = {"cmath": cmath}
    exec(s, g)
    return g["compiled_asm"]

seed = int(OPTIONS["seed"])
print("seed: %d" % seed)
print("registers: %d" % REGNUM)
rng = Random()
rng.seed(seed)

red = optimize(generate(rng))
blue = optimize(generate(rng))
green = optimize(generate(rng))

def print_asm(code):
    for x in code:
        print("%s %s" % (x[0], ", ".join(map(str, x[1:]))))

print("--- red")
print_asm(red)
print("--- green")
print_asm(green)
print("--- blue")
print_asm(blue)

rch = compile_code(red)
gch = compile_code(green)
bch = compile_code(blue)

res = OPTIONS["res"].split("x")
width = int(res[0])
height = int(res[1])

crange = OPTIONS["crange"].split("x")
cxr = float(crange[0])
cyr = float(crange[1])

ccenter = OPTIONS["ccenter"].split("x")
cxc = float(ccenter[0])
cyc = float(ccenter[1])

img = Image.new("RGB", (width, height))

if OPTIONS["autoscale"] == "y":
    if img.width > img.height:
        xscale = width / height
        yscale = 1
    else:
        xscale = 1
        yscale = height / width
else:
    xscale = 1
    yscale = 1
lp = 0
ax_m = 1 / width * 2 * xscale * cxr
ay_m = 1 / height * 2 * yscale * cyr
for x in range(width):
    ax = (x - width / 2) * ax_m + cxc
    for y in range(height):
        ay = (y - height / 2) * ay_m + cyc
        pos = ax+ay*1j
        img.putpixel((x, y), (rch(pos), gch(pos), bch(pos)))
    if x / width > lp + 0.05:
        lp = x / width
        print("%f%% done" % (lp * 100))

img.save("%d-%d-%dx%d-%dx%d-%dx%d.png" % (REGNUM, seed, width, height, cxr, cyr, cxc, cyc))
if OPTIONS["show"] == "y":
    img.show()
